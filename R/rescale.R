#' Scale a vector by its empirical mean and standard deviation
#'
#' @param x numeric. A vector with no missing values of length > 2.
#'
#' @importFrom assertthat assert_that
#'
#' @return a vector with mean centered and unit variance values.
#'
#' @export
#'
#' @examples
#' x <- 1:10
#' rescale(x)

rescale <- function(x) {
  # sanity checks
  assert_that(is.vector(x) && !any(is.na(x)) && all(sapply(x, is.numeric)) && length(x) > 2)

  out <- (x - mean(x)) / sd(x)

  return(out)
}
