#' Compute the rolling mean
#'
#' @param x numeric. A vector (with missing values) of length > 2.
#' @param width count. A positive scalar for the width over which to average values.
#' @param min_sample_size count. A positive scalar for the mininum sample size. Default to width.
#'
#' @importFrom assertthat assert_that is.count
#' @importFrom dplyr lag
#'
#' @return a vector with rolled means.
#'
#' @export
#'
#' @source \url{https://gist.github.com/ateucher/d62f92952bf7367bee6ebdae412679e2}
#'
#' @examples
#' x <- runif(1e2, -1, 1)
#' rolling_mean(x, width = 3)

rolling_mean <- function(x, width, min_sample_size = width) {
  # sanity checks
  assert_that(is.vector(x) && all(sapply(x, is.numeric)) && length(x) > 2)
  assert_that(is.count(width))
  assert_that(is.count(min_sample_size))

  # Inspired by Aman Verma's work in rcaaqs

  available <- !is.na(x)
  x[is.na(x)] <- 0
  rolled_sum <- cumsum(x) - dplyr::lag(cumsum(x), width, default = 0)
  rolled_available <- cumsum(available) - dplyr::lag(cumsum(available), width, default = 0)
  out <- rolled_sum / rolled_available
  out[rolled_available < min_sample_size] <- NA

  return(out)
}
