#' Summary statistics
#'
#' @param x numeric. A vector with no missing values of length > 2.
#' x will be converted to a mcmc object
#' @param alpha confidence level. Default to 0.2.
#'
#' @importFrom assertthat assert_that
#' @importFrom coda HPDinterval as.mcmc
#' @importFrom stats median
#'
#' @return a dataframe with mean, median, standard error, lower and upper bounds.
#'
#' @export
#'
#' @seealso lower, upper
#'
#' @examples
#' x <- rnorm(1e6)
#' get_ci(x)

get_ci <- function(x, alpha = 0.2) {
  # sanity checks
  assert_that(is.vector(x) && !any(is.na(x)) && all(sapply(x, is.numeric)) && length(x) > 2)
  assert_that(is.numeric(alpha) && length(alpha) == 1 && !is.na(alpha) && !is.logical(alpha) && alpha > 0 && alpha < 1)

  out <- data.frame(mean = mean(x),
                    median = median(x),
                    se = sd(x),
                    lower = lower(x, alpha = alpha),
                    upper = upper(x, alpha = alpha)
                    )

  return(out)
}
