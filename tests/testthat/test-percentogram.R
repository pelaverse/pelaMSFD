test_that("percentogram works", {
  x <- rnorm(1e5)
  percentogram_list <- percentogram(y = x)
  expect_length(percentogram_list, 3)
})

test_that("percentogram fails", {
  x <- rnorm(9)
  expect_error(percentogram(y = x))
  x <- rnorm(1e2)
  expect_error(percentogram(y = x, q = -1))
  expect_error(percentogram(y = x, q = 1.1))
})
