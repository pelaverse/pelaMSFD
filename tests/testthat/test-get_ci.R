test_that("get_ci works", {
  expect_equal(class(get_ci(x = rnorm(1e3))), "data.frame")
})

test_that("get_ci fails", {
  expect_error(get_ci(c(1:2)))
  expect_error(get_ci(c(1:2, NA)))
  expect_error(get_ci(x = rnorm(1e3), alpha = 0))
  expect_error(get_ci(x = rnorm(1e3), alpha = 1))
  expect_error(get_ci(x = rnorm(1e3), alpha = NA))
  expect_error(get_ci(x = rnorm(1e3), alpha = -0.1))
  expect_error(get_ci(x = rnorm(1e3), alpha = 1.1))
})
